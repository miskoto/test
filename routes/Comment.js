const express = require('express')
const router = express.Router()

const Comment = require('../models/comment')

//Get All comment
router.get('/comments', (req, res) => {
    Comment.findAll()
        .then(comments => {
            res.json(comments)
        })
        .catch(err => {
            res.send("error: " + err)
        })
})

//Add comment
router.post('/comments', (req, res) => {
    if(!req.body.comment_name) {
        res.status(400)
        res.json({
            error: "Bad data"
        })
    }else {
        Comment.create(req.body)
            .then(() => {
                res.send("Comment Added")
            })
            .catch(err => {
                res.send("Error: " + err)
            })
    }
})

//Delete comment
router.delete('/comments/:id', (req, res) => {
    Comment.destroy({
        where: {
            id: req.params.id
        }
    })
        .then(() => {
            res.send('Comment delete')
        })
        .catch(err => {
            res.send("Error: " + err)
        })
})

module.exports = router