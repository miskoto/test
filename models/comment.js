const Sequelize = require('sequelize');
const db = require('../database/db');

module.exports = db.sequelize.define(
    'comment',
    {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        email: {
            type: Sequelize.STRING
        },

        comment: {
            type: Sequelize.STRING
        },

    },
    {
        timestamps: false
    }
);