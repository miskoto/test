const Sequelize = require("sequelize");
const db = {};
const sequlize = new Sequelize("nodejs_login", "root", "", {
    host: 'localhost',
    dialect: 'mysql',
    pool: {
        max: 5,
        min: 0,
        acquire: 3000,
        idle: 1000
    }
});

db.sequelize = sequlize;
db.Sequelize = Sequelize;

module.exports = db;

