const express = require ('express');
const cors = require ('cors');
const bodyParser = require ('body-parser');

// const passport = require('passport');
// const session = require('express-session');
// const LocalStrategy = require('passport-local');
// const FacebookStrategy = require('passport-facebook');

const app = express();
let port = process.env.PORT || 8000;

app.use(bodyParser.json());
app.use(cors());
app.use(bodyParser.urlencoded({extended: false}));

let Users = require("./routes/Users");
let Comment = require("./routes/Comment");

app.use("/users", Users);
app.use("/comments", Comment);

app.listen(port, () => {
   console.log("Server on " + port)
});

/*
// Конфигурация Passport
app.use(session({secret: 'mySecretKey'}));
app.use(passport.initialize());
app.use(passport.session());*/

//Регистрация через Passport js
/*
const user = {
    email: 'mail@mail.com',
    password: 'Ffff123'
};

let pass = bcrypt.hashSync(user.password, 10); //хэширование пароля

passport.use(new LocalStrategy(
    (email, pass, done) => {
        User.findOne({email: email}, function (err, user) {
            if (err) {
                return done(err);
            }
            if (!user) {
                return done(null, false, {message: 'Incorrect email.'});
            }
            if (!user.validPassword(password)) {
                return done(null, false, {message: 'Incorrect password.'});
            }
            return done(null, user);
        });
    }
));

router.get('/login', function (req, res, next) {
    passport.authenticate('local', function (err, user, info) {
        if (err) {
            return next(err)
        }
        if (!user) {
            return res.redirect('/login')
        }
        req.logIn(user, function (err) {
            if (err) {
                return next(err);
            }
            return res.redirect('/users/' + user.name);
        });
    })(req, res, next);
});
*/

//стратегия аутентификации через Facebook
/*
passport.use(new FacebookStrategy({
        clientID: config.social.facebook.clientID,
        clientSecret: config.social.facebook.clientSecret,
        callbackURL: "http://localhost:3000/login/facebook/callback",
        enableProof: false,
        profileFields: ['id', 'name', 'emails'],
        scope: "email"
    }, (accessToken, refreshToken, profile, done) => {
        User.findOrCreate({facebookId: profile.id}, (err, user) => {
            return cb(err, user);
        });
    }
));

passport.serializeUser((user, done) => {
    done(null, user.email);
});


passport.deserializeUser((email, done) => {
    User.findByEmail(email, function (err, user) {
        err
            ? done(err)
            : done(null, user);
    });
});

router.get('/login/facebook',
    passport.authenticate('facebook'));

router.get('/login/facebook/callback',
    passport.authenticate('facebook', {failureRedirect: '/login'}), (req, res) => {
        res.redirect('/');
    });
*/